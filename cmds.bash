#!/bin/bash


read nick chan saying

. ./.config

# Sends message to current channel
# $1 = message to send
function say()
{
    echo "PRIVMSG $chan :$1"
}

# Sends private message to user
# $1 = Message to send
function privsay()
{
    echo "PRIVMSG ${nick//:} :$1"
}

# Sends pinging message to channel
# $1 = user to ping
# $2 = message to send
function say-to()
{
    echo "PRIVMSG $chan $1: $2"
}

# Usage: has "$thing to search" "regex"
# Returns if thing 2 is found in thing 1
function has()
{
    echo "$1" | grep -Pi "$2" > /dev/null
}

function dadjoke()
{
    joke=$(curl https://icanhazdadjoke.com/)
    header=$(shuf -n 1 ./data/intros.txt)

    say "$header"
    say "$joke"
}


# Private messages
if [[ $chan == $botnick ]] ; then
    if [[ $nick == $admin ]] ; then # Admin commands
        output=`echo "$nick" "$saying" | ./admin.bash`
        echo "$output"
    fi

# Regular channel commands
elif ! grep -Fxqi "${nick//:}" ./data/blacklist.txt && ! `echo "$saying" | grep -Pi '^m[aeiou]ths?(bot)?' > /dev/null` ; then
    if has "$saying" "^${botnick}:? dad\b" ; then
        #joke=$(curl https://icanhazdadjoke.com/)
        #header=$(shuf -n 1 ./data/intros.txt)

        #say "$header"
        #say "$joke"
        dadjoke
    elif has "$saying" "^!dadjoke\b" ; then
        dadjoke

    elif has "$saying" "I'm [A-Za-z]*[\.\!\?]+" ; then
        hungry=$(echo "$saying" | grep -Po "I'm [A-Za-z]*[\.\!\?]+")
        hungry=$(echo "$hungry" | cut -d ' ' -f 2)
        hungry=${hungry:0:-1}

        say "Hi $hungry, I'm dad"
    elif has "$saying" "^${botnick}:? !?source\b" ; then
        say-to "$nick" "Dadbot source code: https://gitlab.com/gilben/dadbot"
    fi
fi
